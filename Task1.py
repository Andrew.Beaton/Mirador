# -*- coding: utf-8 -*-
"""
Created on Tue Feb  2 15:44:31 2021

@author: Andrew
"""

import seaborn as sns
import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt 


colnames=['dog1', 'dog2',"dog3","dog4","dog5","dog6"
                        ,"dog7","dog8"]
dogs = ["grehound","labrador","Collie"]
dat_dog = np.random.choice(a=dogs,size =[50000,8])

dat_dog = pd.DataFrame(dat_dog,columns = colnames)
dat_dog =dat_dog.sort_values(colnames)
melty = pd.melt(dat_dog)
print melty.value.value_counts()
#                       ,"dog7","dog8"]).nunique()

fig1= sns.catplot(x="value",hue="variable",kind="count",data=melty)

#dog_data_melt["value"].value_counts()
#dat_dog.value_counts()
count_zeros = np.zeros([11,1]) #sets up an array of 0's to populate group_count
groups_count =pd.DataFrame(count_zeros,columns=["count"] ,index=["Group Size 1","Group Size 2",
        "Group Size 3","Group Size 4","Group Size 5","Group Size 6","Group Size 7"
        ,"Group Size 8","Group Size 9","Group Size 10","Larger"])

dogcomp= dat_dog.eq(dat_dog.shift(), axis=0) #compares row by row returns boolean


#dogcomp=dogcomp.drop(["index"],axis=1) #Removes index axis

groups=dogcomp.all(axis='columns') #collapses rows to present only deviations as boolean
groups.reset_index(drop=True,inplace=True)

groupsize=0#
previous=True
for index, value in groups.items():
    print index
    if index == 0:
        previous == True
    elif groups[index] == previous :
        groupsize +=1
        print groupsize
        previous = groups[index]
        continue    
    elif groups[index] != previous :
        if groupsize > 10:
            groups_count.loc["Larger","count"] +=1
            groupsize=0
            continue    
        else:
            
            groups_count.iloc[groupsize,0] +=1
            groupsize=0
            continue    
 

groups_count_trimmed=groups_count.drop("Larger") 
g=sns.lineplot(x=groups_count_trimmed.index, y="count", data=groups_count_trimmed, color="c")
plt.xticks(rotation=45)